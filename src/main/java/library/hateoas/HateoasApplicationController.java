
package library.hateoas;


import library.common.model.Application;
import library.hateoas.assembler.ApplicationAssembler;
import library.hateoas.links.LinksApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;

@RestController
@RequestMapping(value = "/library/hateoas",
        produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
public class HateoasApplicationController {

    // API - All GET Requests (CRUD - READ)
    @GetMapping
    public ResponseEntity<LinksApplication> identify(
            final ApplicationAssembler assembler) {
        return ResponseEntity.status(OK)
                .body(assembler.toModel(new Application()));
    }
}
