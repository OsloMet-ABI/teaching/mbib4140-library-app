package library.hateoas;

import library.common.model.BookInstance;
import library.common.service.IBookInstanceService;
import library.hateoas.assembler.BookAssembler;
import library.hateoas.assembler.BookInstanceAssembler;
import library.hateoas.links.LinksBook;
import library.hateoas.links.LinksBookInstance;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;

@RestController
@RequestMapping(value = "/library/hateoas/items",
        produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
public class HateoasBookInstanceController {

    final private IBookInstanceService bookInstanceService;

    public HateoasBookInstanceController(
            IBookInstanceService bookInstanceService) {
        this.bookInstanceService = bookInstanceService;
    }

    @GetMapping
    public ResponseEntity<PagedModel<LinksBookInstance>> getItems(
            final PagedResourcesAssembler<BookInstance> assembler,
            final BookInstanceAssembler bookInstanceAssembler,
            final Pageable pageable) {
        return ResponseEntity.status(OK)
                .body(assembler.toModel(bookInstanceService.findAll(pageable),
                        bookInstanceAssembler));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<LinksBookInstance> getBookInstance(
            final BookInstanceAssembler bookInstanceAssembler,
            @PathVariable("id") UUID id) {
        return ResponseEntity.status(OK).body(
                bookInstanceAssembler.toModel(bookInstanceService.findOne(id)));
    }

    @GetMapping(value = "/{id}/book")
    public ResponseEntity<LinksBook> getBook(
            final BookAssembler bookAssembler,
            @PathVariable("id") UUID id) {
        return ResponseEntity.status(OK).body(
                bookAssembler.toModel(bookInstanceService.findBook(id)));
    }

    @PostMapping
    public ResponseEntity<LinksBookInstance> saveBookInstance(
            final BookInstanceAssembler bookInstanceAssembler,
            @RequestBody BookInstance bookInstance) {
        return ResponseEntity.status(CREATED).body(
                bookInstanceAssembler.toModel(
                        bookInstanceService.save(bookInstance)));
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<LinksBookInstance> updateBookInstance(
            final BookInstanceAssembler bookInstanceAssembler,
            @PathVariable("id") UUID id,
            @RequestBody BookInstance bookInstance) {
        return ResponseEntity.status(OK).body(bookInstanceAssembler.toModel(
                bookInstanceService.update(id, bookInstance)));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> deleteBookInstance(@PathVariable UUID id) {
        bookInstanceService.delete(id);
        return ResponseEntity.status(NO_CONTENT)
                .body("Item with id [" + id + "] was deleted");
    }
}
