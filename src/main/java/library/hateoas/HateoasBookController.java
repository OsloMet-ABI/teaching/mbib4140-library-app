package library.hateoas;

import library.common.model.Author;
import library.common.model.Book;
import library.common.model.BookInstance;
import library.common.service.IBookService;
import library.hateoas.assembler.AuthorAssembler;
import library.hateoas.assembler.BookAssembler;
import library.hateoas.assembler.BookInstanceAssembler;
import library.hateoas.links.LinksAuthor;
import library.hateoas.links.LinksBook;
import library.hateoas.links.LinksBookInstance;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static library.common.utils.Constants.AUTHORS;
import static library.common.utils.Constants.ITEMS;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;

@RestController
@RequestMapping(value = "/library/hateoas/books",
        produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
public class HateoasBookController {

    final private IBookService bookService;

    public HateoasBookController(IBookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping
    public ResponseEntity<PagedModel<LinksBook>> getBooks(
            final PagedResourcesAssembler<Book> assembler,
            final BookAssembler bookAssembler,
            final Pageable pageable) {
        return ResponseEntity.status(OK)
                .body(assembler.toModel(
                        bookService.findAll(pageable), bookAssembler));
    }

    @GetMapping(value = "/{isbn}/" + AUTHORS)
    public ResponseEntity<PagedModel<LinksAuthor>> getBookAuthors(
            final PagedResourcesAssembler<Author> assembler,
            final AuthorAssembler authorAssembler,
            @PageableDefault Pageable pageable,
            @PathVariable("isbn") String isbn) {
        return ResponseEntity.status(OK).body(assembler.toModel(
                bookService.findAuthorsForBook(
                        isbn, pageable), authorAssembler));
    }

    @GetMapping(value = "/{isbn}/" + ITEMS)
    public ResponseEntity<PagedModel<LinksBookInstance>> getBookItems(
            final PagedResourcesAssembler<BookInstance> assembler,
            final BookInstanceAssembler bookInstanceAssembler,
            @PageableDefault final Pageable pageable,
            @PathVariable("isbn") final String isbn) {
        return ResponseEntity.status(OK).body(assembler.toModel(
                bookService.findInstancesOfBook(isbn, pageable),
                bookInstanceAssembler));
    }

    @GetMapping(value = "/{isbn}")
    public ResponseEntity<LinksBook> getBook(
            final BookAssembler bookAssembler,
            @PathVariable("isbn") String isbn) {
        return ResponseEntity.status(OK).body(
                bookAssembler.toModel(bookService.findOne(isbn)));
    }

    @PostMapping
    public ResponseEntity<LinksBook> saveBook(
            final BookAssembler bookAssembler,
            @RequestBody Book book) {
        return ResponseEntity.status(CREATED).body(
                bookAssembler.toModel(bookService.save(book)));
    }

    @PutMapping(value = "/{isbn}")
    public ResponseEntity<LinksBook> updateBook(
            final BookAssembler bookAssembler,
            @PathVariable("isbn") String isbn,
            @RequestBody Book book) {
        return ResponseEntity.status(CREATED).body(
                bookAssembler.toModel(bookService.update(isbn, book)));
    }

    @DeleteMapping(value = "/{isbn}")
    public ResponseEntity<String> deleteBook(@PathVariable String isbn) {
        bookService.delete(isbn);
        return ResponseEntity.status(NO_CONTENT)
                .body("Book with isbn [" + isbn + "] was deleted");
    }
}
