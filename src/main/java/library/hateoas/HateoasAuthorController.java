package library.hateoas;
import library.common.model.Author;
import library.common.service.IAuthorService;
import library.hateoas.assembler.AuthorAssembler;
import library.hateoas.links.LinksAuthor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;

@RestController
@RequestMapping(value = "/library/hateoas/authors",
        produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
public class HateoasAuthorController {

    final private IAuthorService authorService;

    public HateoasAuthorController(IAuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping
    public ResponseEntity<PagedModel<LinksAuthor>> getAuthors(
            final PagedResourcesAssembler<Author> assembler,
            final AuthorAssembler authorAssembler,
            final Pageable pageable) {
        return ResponseEntity.status(OK)
                .body(assembler.toModel(
                        authorService.findAll(pageable), authorAssembler));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<LinksAuthor> getAuthor(
            final AuthorAssembler assembler,
            @PathVariable("id") Long id) {
        return ResponseEntity.status(OK)
                .body(assembler.toModel(authorService.findOne(id)));
    }

    @PostMapping
    public ResponseEntity<LinksAuthor> saveAuthor(
            final AuthorAssembler assembler,
            @RequestBody Author author) {
        return ResponseEntity.status(CREATED)
                .body(assembler.toModel(authorService.save(author)));
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<LinksAuthor> updateAuthor(
            final AuthorAssembler assembler,
            @PathVariable Long id,
            @RequestBody Author author) {
        return ResponseEntity.status(OK)
                .body(assembler.toModel(authorService.update(id, author)));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> deleteAuthor(@PathVariable Long id) {
        authorService.delete(id);
        return ResponseEntity.status(NO_CONTENT)
                .body("Author with id " + id + " was deleted");
    }
}
