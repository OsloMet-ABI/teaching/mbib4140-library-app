package library.hateoas.links;

import com.fasterxml.jackson.annotation.JsonInclude;
import library.common.model.Book;
import lombok.*;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Relation(collectionRelation = "books", itemRelation = "book")
@JsonInclude(NON_NULL)
public class LinksBook
        extends RepresentationModel<LinksBook> {

    private String isbn;
    private String title;

    public LinksBook(Book book) {
        this.isbn = book.getIsbn();
        this.title = book.getTitle();
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
