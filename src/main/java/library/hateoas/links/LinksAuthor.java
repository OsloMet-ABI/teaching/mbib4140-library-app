package library.hateoas.links;

import com.fasterxml.jackson.annotation.JsonInclude;
import library.common.model.Author;
import lombok.*;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Relation(collectionRelation = "authors", itemRelation = "author")
@JsonInclude(NON_NULL)
public class LinksAuthor
        extends RepresentationModel<LinksAuthor> {

    private Long pkId;
    private String authorFirstName;
    private String authorLastName;

    public LinksAuthor(Author author) {
        this.authorFirstName = author.getAuthorFirstName();
        this.authorLastName = author.getAuthorLastName();
    }

    public Long getPkId() {
        return pkId;
    }

    public void setPkId(Long pkId) {
        this.pkId = pkId;
    }

    public String getAuthorFirstName() {
        return authorFirstName;
    }

    public void setAuthorFirstName(String authorFirstName) {
        this.authorFirstName = authorFirstName;
    }

    public String getAuthorLastName() {
        return authorLastName;
    }

    public void setAuthorLastName(String authorLastName) {
        this.authorLastName = authorLastName;
    }
}
