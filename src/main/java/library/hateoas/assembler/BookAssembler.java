package library.hateoas.assembler;

import library.common.model.Book;
import library.hateoas.HateoasBookController;
import library.hateoas.links.LinksBook;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import static library.common.utils.Constants.*;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class BookAssembler
        extends RepresentationModelAssemblerSupport<Book, LinksBook> {

    public BookAssembler() {
        super(HateoasBookController.class, LinksBook.class);
    }

    @Override
    public LinksBook toModel(Book book) {
        LinksBook linksBook = instantiateModel(book);
        linksBook.setIsbn(book.getIsbn());
        linksBook.setTitle(book.getTitle());
        linksBook
                .add(linkTo(methodOn(HateoasBookController.class)
                        .getBook(null, book.getIsbn()))
                        .withSelfRel());
        linksBook
                .add(linkTo(methodOn(HateoasBookController.class)
                        .getBooks(null, null, null))
                        .withRel(BOOKS));
        linksBook
                .add(linkTo(methodOn(HateoasBookController.class)
                        .getBookItems(null, null, null, book.getIsbn()))
                        .withRel(ITEMS));
        linksBook
                .add(linkTo(methodOn(HateoasBookController.class)
                        .getBookAuthors(null, null, null, book.getIsbn()))
                        .withRel(AUTHORS));
        return linksBook;
    }

    @Override
    public CollectionModel<LinksBook> toCollectionModel(
            Iterable<? extends Book> books) {
        CollectionModel<LinksBook> linksBooks =
                super.toCollectionModel(books);

        linksBooks.forEach(book -> {
            book.add(linkTo(methodOn(HateoasBookController.class)
                    .getBook(null, book.getIsbn()))
                    .withSelfRel());
            book.add(linkTo(methodOn(HateoasBookController.class)
                    .getBooks(null, null, null))
                    .withRel(BOOKS));
            book.add(linkTo(methodOn(HateoasBookController.class)
                    .getBookItems(null, null, null, book.getIsbn()))
                    .withRel(ITEMS));
            book.add(linkTo(methodOn(HateoasBookController.class)
                    .getBookAuthors(null, null, null, book.getIsbn()))
                    .withRel(AUTHORS));
        });
        return linksBooks;
    }
}
