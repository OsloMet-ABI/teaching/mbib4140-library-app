package library.hateoas.assembler;

import library.common.model.BookInstance;
import library.hateoas.HateoasBookInstanceController;
import library.hateoas.links.LinksBookInstance;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import static library.common.utils.Constants.ITEMS;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class BookInstanceAssembler
        extends
        RepresentationModelAssemblerSupport<BookInstance, LinksBookInstance> {

    public BookInstanceAssembler() {
        super(HateoasBookInstanceController.class, LinksBookInstance.class);
    }

    @Override
    public LinksBookInstance toModel(BookInstance bookInstance) {
        LinksBookInstance linksBookInstance = instantiateModel(bookInstance);
        linksBookInstance.setId(bookInstance.getId());
        linksBookInstance.setOnLoan(bookInstance.getOnLoan());
        linksBookInstance.setType(bookInstance.getType());
        linksBookInstance.setReturnDate(bookInstance.getReturnDate());
        linksBookInstance
                .add(linkTo(methodOn(HateoasBookInstanceController.class)
                        .getBookInstance(null, bookInstance.getId()))
                        .withSelfRel());
        linksBookInstance
                .add(linkTo(methodOn(HateoasBookInstanceController.class)
                        .getBook(null, bookInstance.getId()))
                        .withRel(ITEMS));
        return linksBookInstance;
    }

    @Override
    public CollectionModel<LinksBookInstance> toCollectionModel(
            Iterable<? extends BookInstance> bookInstances) {
        CollectionModel<LinksBookInstance> linksBookInstances =
                super.toCollectionModel(bookInstances);

        linksBookInstances.forEach(bookInstance -> {
            bookInstance
                    .add(linkTo(methodOn(HateoasBookInstanceController.class)
                            .getBookInstance(null, bookInstance.getId()))
                            .withSelfRel());
            bookInstance
                    .add(linkTo(methodOn(HateoasBookInstanceController.class)
                            .getBook(null, bookInstance.getId()))
                            .withRel(ITEMS));
        });
        return linksBookInstances;
    }
}
