package library.hateoas.assembler;

import library.common.model.Author;
import library.hateoas.HateoasAuthorController;
import library.hateoas.links.LinksAuthor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class AuthorAssembler
        extends RepresentationModelAssemblerSupport<Author, LinksAuthor> {

    public AuthorAssembler() {
        super(HateoasAuthorController.class, LinksAuthor.class);
    }

    @Override
    public LinksAuthor toModel(Author author) {
        LinksAuthor linksAuthor = instantiateModel(author);
        linksAuthor.setAuthorFirstName(author.getAuthorFirstName());
        linksAuthor.setAuthorLastName(author.getAuthorLastName());
        linksAuthor
                .add(linkTo(methodOn(HateoasAuthorController.class)
                        .getAuthor(null, author.getPkId()))
                        .withSelfRel());
        linksAuthor
                .add(linkTo(methodOn(HateoasAuthorController.class)
                        .getAuthors(null, null, null))
                        .withSelfRel());
        return linksAuthor;
    }

    @Override
    public CollectionModel<LinksAuthor> toCollectionModel(
            Iterable<? extends Author> authors) {
        CollectionModel<LinksAuthor> linksAuthors =
                super.toCollectionModel(authors);

        linksAuthors.forEach(author -> {
            author.add(linkTo(methodOn(HateoasAuthorController.class)
                    .getAuthor(null, author.getPkId()))
                    .withSelfRel());
            author.add(linkTo(methodOn(HateoasAuthorController.class)
                    .getAuthors(null, null, null))
                    .withSelfRel());
        });
        return linksAuthors;
    }
}
