package library.hateoas.assembler;

import library.common.model.Application;
import library.hateoas.HateoasApplicationController;
import library.hateoas.HateoasAuthorController;
import library.hateoas.HateoasBookController;
import library.hateoas.links.LinksApplication;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import static library.common.utils.Constants.AUTHORS;
import static library.common.utils.Constants.BOOKS;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class ApplicationAssembler
        extends RepresentationModelAssemblerSupport<Application, LinksApplication> {

    public ApplicationAssembler() {
        super(HateoasApplicationController.class, LinksApplication.class);
    }

    @Override
    public LinksApplication toModel(Application application) {
        LinksApplication linksApplication = instantiateModel(application);
        linksApplication
                .add(linkTo(methodOn(HateoasApplicationController.class)
                        .identify(null))
                        .withSelfRel());
        linksApplication.add(linkTo(methodOn(HateoasAuthorController.class)
                .getAuthors(null, null, null))
                .withRel(AUTHORS));
        linksApplication.add(linkTo(methodOn(HateoasBookController.class)
                .getBooks(null, null, null))
                .withRel(BOOKS));
        return linksApplication;
    }

    @Override
    public CollectionModel<LinksApplication> toCollectionModel(
            Iterable<? extends Application> applications) {
        CollectionModel<LinksApplication> linksApplications =
                super.toCollectionModel(applications);

        linksApplications.forEach(application -> {
            application.add(linkTo(methodOn(HateoasApplicationController.class)
                    .identify(null))
                    .withSelfRel());
        });
        return linksApplications;
    }
}
