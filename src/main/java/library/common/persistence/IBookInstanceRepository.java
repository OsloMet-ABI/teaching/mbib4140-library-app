package library.common.persistence;

import library.common.model.Book;
import library.common.model.BookInstance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;
import java.util.UUID;

@Repository
public interface IBookInstanceRepository
        extends PagingAndSortingRepository<BookInstance, UUID> {

    @Override
    BookInstance save(BookInstance bookInstance);

    @Override
    Set<BookInstance> findAll();

    @Override
    Page<BookInstance> findAll(Pageable pageable);

    Page<BookInstance> findAllByReferenceBook(Book book, Pageable pageable);
}
