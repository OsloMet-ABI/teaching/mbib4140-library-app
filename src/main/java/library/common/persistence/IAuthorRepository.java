package library.common.persistence;

import library.common.model.Author;
import library.common.model.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface IAuthorRepository extends CrudRepository<Author, Long> {

    @Override
    Author save(Author Author);

    @Override
    Set<Author> findAll();

    Page<Author> findAll(Pageable pageable);

    Page<Author> findByBooks(Book book, Pageable pageable);

    // authorFirstName
    Set<Author> findByAuthorFirstName(String authorFirstName);

    // authorLastName
    Set<Author> findByAuthorLastName(String authorLastName);

    // authorFirstName
    Page<Author> findByAuthorFirstName(String authorFirstName, Pageable pageable);

    // authorLastName
    Page<Author> findByAuthorLastName(String authorLastName, Pageable pageable);
}
