package library.common.persistence;

import library.common.model.Book;
import library.common.model.BookInstance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface IBookRepository
        extends PagingAndSortingRepository<Book, String> {

    @Override
    Book save(Book book);

    @Override
    Set<Book> findAll();

    @Override
    Page<Book> findAll(Pageable pageable);

    Set<Book> findByTitleContaining(String title);

    // find partial by title bookLastName
    Page<Book> findByTitleContaining(String title, Pageable pageable);

    Book findByReferenceBookInstance(BookInstance bookInstance);
}
