package library.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.time.OffsetDateTime;
import java.util.UUID;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;
import static library.common.utils.Constants.*;
import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME;

@XmlAccessorType(FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "onLoan"
})
@Entity
@Table(name = BOOK_INSTANCE)
@XmlRootElement(name = BOOK, namespace="http://abi.oslomet.no/types/library")
public class BookInstance {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator",
            parameters = {@Parameter(
                    name = "uuid_gen_strategy_class",
                    value = "org.hibernate.id.uuid.CustomVersionOneStrategy")})
    @Column(name = COL_BOOK_ID, updatable = false, nullable = false)
    @Type(type = "uuid-char")
    @XmlElement(name = "bookId", required = true)
    private UUID id;

    @Column
    private Boolean onLoan;

    @Column
    private String type;

    @Column
    @DateTimeFormat(iso = DATE_TIME)
    private OffsetDateTime returnDate;

    @ManyToOne
    @JoinColumn(name = FK_BOOK, referencedColumnName = COL_BOOK_ISBN)
    @JsonIgnore
    private Book referenceBook;

    public UUID getId() {
        return id;
    }

    public Boolean getOnLoan() {
        return onLoan;
    }

    public void setOnLoan(Boolean onLoan) {
        this.onLoan = onLoan;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public OffsetDateTime getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(OffsetDateTime returnDate) {
        this.returnDate = returnDate;
    }

    public Book getReferenceBook() {
        return referenceBook;
    }

    public void setReferenceBook(Book referenceBook) {
        this.referenceBook = referenceBook;
    }
}
