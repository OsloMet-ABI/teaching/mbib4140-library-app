package library.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.HashSet;
import java.util.Set;

import static library.common.utils.Constants.BOOK;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="isbn" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "title",
        "isbn"
})
/**
 * Created by tsodring on 9/25/17.
 */
@Entity
@Table(name = "books")
@XmlRootElement(name = BOOK, namespace = "http://abi.oslomet.no/types/library")
public class Book {

    @Id
    @XmlElement(name = "ISBN", required = true)
    @Column(name = "isbn")
    private String isbn;

    @XmlElement(name = "title", required = true)
    @Column(name = "title")
    private String title;

    @XmlTransient
    @ManyToMany(fetch = FetchType.EAGER,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "book_authors",
            joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id")
    )
    @JsonIgnore
    private Set<Author> authors = new HashSet<>();

    // Links to BookInstance
    @OneToMany(mappedBy = "referenceBook")
    @JsonIgnore
    private Set<BookInstance> referenceBookInstance = new HashSet<>();

    public Book() {
    }

    public String getTitle() {
        return title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

    public Set<BookInstance> getReferenceBookInstance() {
        return referenceBookInstance;
    }

    public void setReferenceBookInstance(Set<BookInstance> referenceBookInstance) {
        this.referenceBookInstance = referenceBookInstance;
    }
}
