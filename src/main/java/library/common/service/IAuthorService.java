package library.common.service;

import library.common.model.Author;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Set;

/**
 * Created by tsodring on 9/25/17.
 */
public interface IAuthorService {
    Set findAll();

    Page<Author> findAll(Pageable pageable);

    Author findOne(Long id);

    Author save(Author author);

    Author update(Long id, Author author);

    void delete(Long id);

    // authorFirstName
    Set<Author> findByAuthorFirstName(String authorFirstName);

    // authorLastName
    Set<Author> findByAuthorLastName(String authorLastName);

    Page<Author> findByAuthorFirstName(
            String authorFirstName, Pageable pageable);

    Page<Author> findByAuthorLastName(
            String authorLastName, Pageable pageable);
}
