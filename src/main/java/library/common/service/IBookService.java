package library.common.service;


import library.common.model.Author;
import library.common.model.Book;
import library.common.model.BookInstance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Set;

/**
 * Created by tsodring on 9/25/17.
 */
public interface IBookService {
    Book findOne(String isbn);

    Page<Book> findAll(Pageable pageable);

    Set<Book> findAll();

    Book save(Book book);

    Book update(String isbn, Book book);

    void delete(String isbn);

    Set<Book> findByTitle(String title);

    Page<Book> findByTitle(String title, Pageable pageable);

    Page<Author> findAuthorsForBook(String isbn, Pageable pageable);

    Set<Author> findAuthorsForBook(String isbn);

    Page<BookInstance> findInstancesOfBook(String isbn, Pageable pageable);

    Set<BookInstance> findInstancesOfBook(String isbn);
}
