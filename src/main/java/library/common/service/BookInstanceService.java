package library.common.service;

import library.common.model.Book;
import library.common.model.BookInstance;
import library.common.persistence.IBookInstanceRepository;
import library.common.persistence.IBookRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import javax.validation.constraints.NotNull;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Service
@Transactional
public class BookInstanceService
        implements IBookInstanceService {

    final private IBookInstanceRepository bookInstanceRepository;
    final private IBookRepository bookRepository;

    public BookInstanceService(IBookInstanceRepository bookInstanceRepository,
                               IBookRepository bookRepository) {
        this.bookInstanceRepository = bookInstanceRepository;
        this.bookRepository = bookRepository;
    }

    @Override
    public BookInstance findOne(UUID id) {
        return getBookInstanceOrThrow(id);
    }

    @Override
    public Page<BookInstance> findAll(Pageable pageable) {
        return bookInstanceRepository.findAll(pageable);
    }

    @Override
    public Set<BookInstance> findAll() {
        return bookInstanceRepository.findAll();
    }

    @Override
    public BookInstance save(BookInstance bookInstance) {
        return bookInstanceRepository.save(bookInstance);
    }

    @Override
    public BookInstance update(UUID id, BookInstance bookInstance) {
        BookInstance originalBookInstance = getBookInstanceOrThrow(id);
        originalBookInstance.setOnLoan(bookInstance.getOnLoan());
        originalBookInstance.setType(bookInstance.getType());
        return originalBookInstance;
    }

    @Override
    public void delete(UUID id) {
        bookInstanceRepository.delete(getBookInstanceOrThrow(id));
    }

    @Override
    public Book findBook(UUID id) {
        return bookRepository.findByReferenceBookInstance(
                getBookInstanceOrThrow(id));
    }

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. If you call this, be aware
     * that you will only ever get a valid BookInstance back. If there is no
     * valid BookInstance, an exception is thrown
     *
     * @param id id of the bookInstance to find.
     * @return the bookInstance
     */
    protected BookInstance getBookInstanceOrThrow(
            @NotNull UUID id) {
        Optional<BookInstance> bookInstanceOptional =
                bookInstanceRepository.findById(id);
        if (bookInstanceOptional.isEmpty()) {
            String info = "Cannot find item with id [" + id + "]";
            throw new EntityNotFoundException(info);
        }
        return bookInstanceOptional.get();
    }
}
