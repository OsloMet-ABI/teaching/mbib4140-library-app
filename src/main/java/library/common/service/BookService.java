package library.common.service;

import library.common.model.Author;
import library.common.model.Book;
import library.common.model.BookInstance;
import library.common.persistence.IAuthorRepository;
import library.common.persistence.IBookInstanceRepository;
import library.common.persistence.IBookRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import javax.validation.constraints.NotNull;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
public class BookService
        implements IBookService {

    final private IBookRepository bookRepository;
    final private IBookInstanceRepository bookInstanceRepository;
    final private IAuthorRepository authorRepository;

    public BookService(IBookRepository bookRepository,
                       IBookInstanceRepository bookInstanceRepository,
                       IAuthorRepository authorRepository) {
        this.bookRepository = bookRepository;
        this.bookInstanceRepository = bookInstanceRepository;
        this.authorRepository = authorRepository;
    }

    public Book findOne(String isbn) {
        return getBookOrThrow(isbn);
    }

    @Override
    public Page<Book> findAll(Pageable pageable) {
        return bookRepository.findAll(pageable);
    }

    @Override
    public Set<Book> findAll() {
        return bookRepository.findAll();
    }

    @Override
    public Book save(Book book) {
        return bookRepository.save(book);
    }

    @Override
    public Book update(String isbn, Book book) {
        Book originalBook = getBookOrThrow(isbn);
        originalBook.setTitle(book.getTitle());
        return originalBook;
    }

    @Override
    public void delete(String isbn) {
        bookRepository.delete(getBookOrThrow(isbn));
    }

    @Override
    public Set<Book> findByTitle(String title) {
        return bookRepository.findByTitleContaining(title);
    }

    @Override
    public Page<Book> findByTitle(String title, Pageable pageable) {
        return bookRepository.findByTitleContaining(title, pageable);
    }

    @Override
    public Page<Author> findAuthorsForBook(String isbn, Pageable pageable) {
        return authorRepository.findByBooks(getBookOrThrow(isbn), pageable);
    }

    @Override
    public Set<Author> findAuthorsForBook(String isbn) {
        return getBookOrThrow(isbn).getAuthors();
    }

    @Override
    public Page<BookInstance> findInstancesOfBook(
            String isbn, Pageable pageable) {
        return bookInstanceRepository.findAllByReferenceBook(
                getBookOrThrow(isbn), pageable);
    }

    @Override
    public Set<BookInstance> findInstancesOfBook(String isbn) {
        return getBookOrThrow(isbn).getReferenceBookInstance();
    }

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. If you call this, be aware
     * that you will only ever get a valid Book back. If there is no
     * valid Book, an exception is thrown
     *
     * @param isbn isbn of the book to find.
     * @return the book
     */
    protected Book getBookOrThrow(
            @NotNull String isbn) {
        Optional <Book> bookOptional =
                bookRepository.findById(isbn);
        if (bookOptional.isEmpty()) {
            String info = "Cannot find entity of type book with isbn ["
            + isbn + "]";
            throw new EntityNotFoundException(info);
        }
        return bookOptional.get();
    }
}
