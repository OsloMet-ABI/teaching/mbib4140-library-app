package library.common.service;

import library.common.model.Author;
import library.common.persistence.IAuthorRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import javax.validation.constraints.NotNull;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
public class AuthorService
        implements IAuthorService {

    final private IAuthorRepository authorRepository;

    public AuthorService(IAuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public Set<Author> findAll() {
        return authorRepository.findAll();
    }

    @Override
    public Page<Author> findAll(Pageable pageable) {
        return authorRepository.findAll(pageable);
    }

    @Override
    public Author findOne(Long id) {
        return getAuthorOrThrow(id);
    }

    @Override
    public Author save(Author author) {
        return authorRepository.save(author);
    }

    @Override
    public Author update(Long id, Author author) {
        Author originalAuthor = getAuthorOrThrow(id);
        originalAuthor.setAuthorFirstName(author.getAuthorFirstName());
        originalAuthor.setAuthorLastName(author.getAuthorLastName());
        return originalAuthor;
    }
    
    @Override
    public void delete(Long id) {
        authorRepository.delete(getAuthorOrThrow(id));
    }

    @Override
    public Set<Author> findByAuthorFirstName(String authorFirstName) {
        return authorRepository.findByAuthorFirstName(authorFirstName);
    }

    @Override
    public Set<Author> findByAuthorLastName(String authorLastName) {
        return authorRepository.findByAuthorLastName(authorLastName);
    }

    @Override
    public Page<Author> findByAuthorFirstName(
            String authorFirstName, Pageable pageable) {
        return authorRepository.findByAuthorFirstName(authorFirstName, pageable);
    }

    @Override
    public Page<Author> findByAuthorLastName(
            String authorLastName, Pageable pageable) {
        return authorRepository.findByAuthorLastName(authorLastName, pageable);
    }

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. If you call this, be aware
     * that you will only ever get a valid Author back. If there is no
     * valid Author, an exception is thrown
     *
     * @param id id of the Author to find.
     * @return the Author
     */
    protected Author getAuthorOrThrow(
            @NotNull Long id) {
        Optional<Author> AuthorOptional = authorRepository.findById(id);
        if (AuthorOptional.isEmpty()) {
            String info = "Cannot find entity of type Author with id ["
                    + id + "]";
            throw new EntityNotFoundException(info);
        }
        return AuthorOptional.get();
    }
}
