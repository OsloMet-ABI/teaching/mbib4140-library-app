package library.common.service;

import library.common.model.Book;
import library.common.model.BookInstance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Set;
import java.util.UUID;

public interface IBookInstanceService {
    BookInstance findOne(UUID id);

    Page<BookInstance> findAll(Pageable pageable);

    Set<BookInstance> findAll();

    BookInstance save(BookInstance bookInstance);

    BookInstance update(UUID id, BookInstance bookInstance);

    void delete(UUID id);

    Book findBook(UUID id);
}
