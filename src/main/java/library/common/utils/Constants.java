package library.common.utils;

/**
 * Application constants.
 */
public final class Constants {

    public static final String HATEOAS_API_PATH = "/library/hateoas";
    public static final String SLASH = "/";
    public static final String LINKS = "_links";
    public static final String HREF = "href";
    public static final String REL = "rel";
    public static final String TEMPLATED = "templated";
    public static final String SELF = "self";
    public static final String PKID = "pkId";
    public static final String AUTHOR = "author";
    public static final String AUTHOR_FIRST_NAME = "authorFirstName";
    public static final String AUTHOR_LAST_NAME = "authorLastName";
    public static final String BOOK = "book";
    public static final String BOOK_INSTANCE = "book_instance";
    public static final String BOOK_TITLE = "title";
    public static final String BOOK_ISBN = "isbn";
    public static final String COL_BOOK_ISBN = "isbn";
    public static final String COL_BOOK_ID = "book_id";
    public static final String FK_BOOK = "fk_book";

    // Hateoas constants
    public static final String AUTHORS = "authors";
    public static final String BOOKS = "books";
    public static final String ITEMS = "items";
}
