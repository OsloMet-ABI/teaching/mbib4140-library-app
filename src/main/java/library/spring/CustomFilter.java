package library.spring;

import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * Taken from:
 * https://github.com/eugenp/tutorials/blob/8bc79559acf2aad6736afe1e23049a9b29c64115/spring-security-modules/spring-security-web-rest-basic-auth/src/main/java/com/baeldung/filter/CustomFilter.java
 */
public class CustomFilter extends GenericFilterBean {

    @Override
    public void doFilter(ServletRequest request,
                         ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        chain.doFilter(request, response);
    }
}
