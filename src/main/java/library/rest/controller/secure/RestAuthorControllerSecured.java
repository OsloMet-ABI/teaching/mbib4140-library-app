
package library.rest.controller.secure;

import library.common.model.Author;
import library.common.service.IAuthorService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/library/secure-rest/authors")
public class RestAuthorControllerSecured {

    final private IAuthorService authorService;

    RestAuthorControllerSecured(IAuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping
    public Set<Author> getAuthors() {
        return authorService.findAll();
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Author> getAuthor(@PathVariable("id") Long id) {
        return ResponseEntity.status(OK)
                .body(authorService.findOne(id));
    }

    @PostMapping
    public ResponseEntity<Author> saveAuthor(@RequestBody Author author) {
        return ResponseEntity.status(CREATED)
                .body(authorService.save(author));
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Author> updateBook(
            @PathVariable("id") Long id,
            @RequestBody Author author) throws Exception {
        return ResponseEntity.status(OK)
                .body(authorService.update(id, author));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> deleteAuthor(@PathVariable Long id) {
        authorService.delete(id);
        return ResponseEntity.status(NO_CONTENT)
                .body("Author with id " + id + " was deleted");
    }
}
