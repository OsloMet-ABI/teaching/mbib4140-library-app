package library.rest.controller.secure;

import library.common.model.Author;
import library.common.model.Book;
import library.common.model.BookInstance;
import library.common.service.IBookService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/library/secure-rest")
public class RestBookControllerSecured {

    final private IBookService bookService;

    RestBookControllerSecured(IBookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping(value = "books")
    public Set<Book> getBooks() {
        return bookService.findAll();
    }

    @GetMapping(value = "books", params = "title")
    public Set<Book> getBooksByTitleSearch(
            @RequestParam("title") String title) {
        return bookService.findByTitle(title);
    }

    @GetMapping(value = "/book/{isbn}")
    public ResponseEntity<Book> getBook(
            @PathVariable("isbn") String isbn) {
        return ResponseEntity.status(OK)
                .body(bookService.findOne(isbn));
    }

    @GetMapping(value = "/book/{isbn}/authors")
    public ResponseEntity<Set<Author>> getBookAuthors(
            @PathVariable("isbn") String isbn) throws Exception {
        return ResponseEntity.status(OK)
                .body(bookService.findAuthorsForBook(isbn));
    }

    @GetMapping(value = "/book/{isbn}/books")
    public ResponseEntity<Set<BookInstance>> getBookInstances(
            @PathVariable("isbn") String isbn) {
        return ResponseEntity.status(OK)
                .body(bookService.findInstancesOfBook(isbn));
    }

    @PostMapping(value = "/book")
    public ResponseEntity<Book> createBook(@RequestBody Book book) {
        return ResponseEntity.status(CREATED)
                .body(bookService.save(book));
    }

    @PutMapping(value = "/book/{isbn}")
    public ResponseEntity<Book> updateBook(
            @PathVariable("isbn") String isbn, @RequestBody Book book)
            throws Exception {
        return ResponseEntity.status(OK)
                .body(bookService.update(isbn, book));
    }

    @DeleteMapping(value = "/book/{isbn}")
    public ResponseEntity<String> deleteBook(@PathVariable String isbn) {
        bookService.delete(isbn);
        return ResponseEntity.status(NO_CONTENT)
                .body("Book with isbn " + isbn + " was deleted");
    }
}
