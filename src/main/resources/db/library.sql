INSERT INTO authors (author_id, author_first_name, author_last_name) VALUES (1, 'Ethan', 'Cerami');
INSERT INTO authors (author_id, author_first_name, author_last_name) VALUES (2, 'Randall', 'Monroe');

INSERT INTO books (title, isbn) VALUES ('Web Services Essentials', '97805960022441');
INSERT INTO books (title, isbn) VALUES ('XKCD Volume 0', '9780615314464');

INSERT INTO book_authors (author_id, book_id) VALUES (2, '97805960022441');
INSERT INTO book_authors (author_id, book_id) VALUES (1, '9780615314464');

INSERT INTO book_instance (book_id, on_loan, fk_book) VALUES ('ccef3b3e-49ca-4ba4-8636-b0db758c53bc', false, '97805960022441');
INSERT INTO book_instance (book_id, on_loan, fk_book) VALUES ('4aabb04c-8013-4c81-8bc2-f896bb9549d5', false, '97805960022441');
INSERT INTO book_instance (book_id, on_loan, fk_book) VALUES ('7c61d923-121d-4355-ba85-931811173c5a', false, '9780615314464');
INSERT INTO book_instance (book_id, on_loan, fk_book) VALUES ('66bc54c4-8b9e-41db-8fd1-143b3b1c6e3e', false, '9780615314464');
INSERT INTO book_instance (book_id, on_loan, fk_book) VALUES ('ff9e0077-2e09-4906-a54d-35210b6a6e1f', false, '9780615314464');
